- name: setup the bharatmoocapp env
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"
  template: >
    src=bharatmoocapp_env.j2 dest={{ bharatmoocapp_app_dir }}/bharatmoocapp_env
    owner={{ bharatmoocapp_user }}  group={{ common_web_user }}
    mode=0644

# Optional auth for git
- name: create ssh script for git (not authenticated)
  template: >
    src=git_ssh_noauth.sh.j2 dest={{ bharatmoocapp_git_ssh }}
    owner={{ bharatmoocapp_user }} mode=750
  when: not BHARATMOOCAPP_USE_GIT_IDENTITY

- name: create ssh script for git (authenticated)
  template: >
    src=git_ssh_auth.sh.j2 dest={{ bharatmoocapp_git_ssh }}
    owner={{ bharatmoocapp_user }} mode=750
  when: BHARATMOOCAPP_USE_GIT_IDENTITY

- name: install read-only ssh key
  copy: >
    content="{{ BHARATMOOCAPP_GIT_IDENTITY }}" dest={{ bharatmoocapp_git_identity }}
    force=yes owner={{ bharatmoocapp_user }} mode=0600
  when: BHARATMOOCAPP_USE_GIT_IDENTITY

# Do A Checkout
- name: checkout bharatmooc-platform repo into {{bharatmoocapp_code_dir}}
  git: >
    dest={{bharatmoocapp_code_dir}} repo={{bharatmooc_platform_repo}} version={{bharatmooc_platform_version}}
    accept_hostkey=yes
  register: chkout
  sudo_user: "{{ bharatmoocapp_user }}"
  environment:
    GIT_SSH: "{{ bharatmoocapp_git_ssh }}"
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"

- name: git clean after checking out bharatmooc-platform
  shell: cd {{bharatmoocapp_code_dir}} && git clean -xdf
  sudo_user: "{{ bharatmoocapp_user }}"
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"

- name: checkout theme
  git: >
    dest={{ bharatmoocapp_app_dir }}/themes/{{bharatmoocapp_theme_name}} repo={{bharatmoocapp_theme_source_repo}} version={{bharatmoocapp_theme_version}}
    accept_hostkey=yes
  when: bharatmoocapp_theme_name != ''
  sudo_user: "{{ bharatmoocapp_user }}"
  environment:
    GIT_SSH: "{{ bharatmoocapp_git_ssh }}"
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"

- name: create checksum for requirements, package.json and Gemfile
  shell: >
    /usr/bin/md5sum {{ " ".join(bharatmoocapp_chksum_req_files) }} 2>/dev/null > /var/tmp/bharatmoocapp.req.new
  sudo_user: "{{ bharatmoocapp_user }}"
  ignore_errors: true

- stat: path=/var/tmp/bharatmoocapp.req.new
  register: new
  sudo_user: "{{ bharatmoocapp_user }}"

- stat: path=/var/tmp/bharatmoocapp.req.installed
  register: inst
  sudo_user: "{{ bharatmoocapp_user }}"

# Substitute github mirror in all requirements files
# This is run on every single deploy
- name: Updating requirement files for git mirror
  command: |
    /bin/sed -i -e 's/github\.com/{{ COMMON_GIT_MIRROR }}/g' {{ " ".join(bharatmoocapp_all_req_files) }}
  sudo_user: "{{ bharatmoocapp_user }}"
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"

# Ruby plays that need to be run after platform updates.
- name: gem install bundler
  shell: >
    gem install bundle
      chdir={{ bharatmoocapp_code_dir }}
      executable=/bin/bash
  environment: "{{ bharatmoocapp_environment }}"
  sudo_user: "{{ bharatmoocapp_user }}"
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"

- name: bundle install
  shell: >
    bundle install --binstubs
      chdir={{ bharatmoocapp_code_dir }}
      executable=/bin/bash
  sudo_user: "{{ bharatmoocapp_user }}"
  environment: "{{ bharatmoocapp_environment }}"
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"

# Set the npm registry
- name: Set the npm registry
  shell:
    npm config set registry '{{ COMMON_NPM_MIRROR_URL }}'
    creates="{{ bharatmoocapp_app_dir }}/.npmrc"
  sudo_user: "{{ bharatmoocapp_user }}"
  environment: "{{ bharatmoocapp_environment }}"
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"

# Node play that need to be run after platform updates.
- name: Install bharatmooc-platform npm dependencies
  shell: npm install chdir={{ bharatmoocapp_code_dir }}
  sudo_user: "{{ bharatmoocapp_user }}"
  environment: "{{ bharatmoocapp_environment }}"
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"


# Install the python pre requirements into {{ bharatmoocapp_venv_dir }}
- name : install python pre-requirements
  pip: >
    requirements="{{pre_requirements_file}}"
    virtualenv="{{bharatmoocapp_venv_dir}}"
    state=present
    extra_args="-i {{ COMMON_PYPI_MIRROR_URL }}"
  sudo_user: "{{ bharatmoocapp_user }}"
  environment: "{{ bharatmoocapp_environment }}"
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"
  when: not inst.stat.exists or new.stat.md5 != inst.stat.md5

# Install the python modules into {{ bharatmoocapp_venv_dir }}
- name : install python base-requirements
  # Need to use shell rather than pip so that we can maintain the context of our current working directory; some
  # requirements are pathed relative to the bharatmooc-platform repo. Using the pip from inside the virtual environment implicitly
  # installs everything into that virtual environment.
  shell: >
    {{ bharatmoocapp_venv_dir }}/bin/pip install -i {{ COMMON_PYPI_MIRROR_URL }} --exists-action w --use-mirrors -r {{ base_requirements_file }}
    chdir={{ bharatmoocapp_code_dir }}
  environment: "{{ bharatmoocapp_environment }}"
  sudo_user: "{{ bharatmoocapp_user }}"
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"
  when: not inst.stat.exists or new.stat.md5 != inst.stat.md5

# Install the python post requirements into {{ bharatmoocapp_venv_dir }}
- name : install python post-requirements
  pip: >
    requirements="{{post_requirements_file}}"
    virtualenv="{{bharatmoocapp_venv_dir}}"
    state=present
    extra_args="-i {{ COMMON_PYPI_MIRROR_URL }}"
  sudo_user: "{{ bharatmoocapp_user }}"
  environment: "{{ bharatmoocapp_environment }}"
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"
  when: not inst.stat.exists or new.stat.md5 != inst.stat.md5

# Install the python paver requirements into {{ bharatmoocapp_venv_dir }}
- name : install python paver-requirements
  pip: >
    requirements="{{paver_requirements_file}}"
    virtualenv="{{bharatmoocapp_venv_dir}}"
    state=present
    extra_args="-i {{ COMMON_PYPI_MIRROR_URL }}"
  sudo_user: "{{ bharatmoocapp_user }}"
  environment: "{{ bharatmoocapp_environment }}"
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"
  when: not inst.stat.exists or new.stat.md5 != inst.stat.md5

# Install the final python modules into {{ bharatmoocapp_venv_dir }}
- name : install python post-post requirements
  # Need to use shell rather than pip so that we can maintain the context of our current working directory; some
  # requirements are pathed relative to the bharatmooc-platform repo. Using the pip from inside the virtual environment implicitly
  # installs everything into that virtual environment.
  shell: >
    {{ bharatmoocapp_venv_dir }}/bin/pip install -i {{ COMMON_PYPI_MIRROR_URL }} --exists-action w --use-mirrors -r {{ item }}
    chdir={{ bharatmoocapp_code_dir }}
  with_items:
  - "{{ github_requirements_file }}"
  - "{{ local_requirements_file }}"
  sudo_user: "{{ bharatmoocapp_user }}"
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"

# Private requriements require a ssh key to install, use the same key as the private key for bharatmooc-platform
# If BHARATMOOCAPP_INSTALL_PRIVATE_REQUIREMENTS is set to true BHARATMOOCAPP_USE_GIT_IDENTITY must also be true
- name : install python private requirements
  # Need to use shell rather than pip so that we can maintain the context of our current working directory; some
  # requirements are pathed relative to the bharatmooc-platform repo. Using the pip from inside the virtual environment implicitly
  # installs everything into that virtual environment.
  shell: >
    {{ bharatmoocapp_venv_dir }}/bin/pip install -i {{ COMMON_PYPI_MIRROR_URL }} --exists-action w --use-mirrors -r {{ item }}
    chdir={{ bharatmoocapp_code_dir }}
  with_items:
  - "{{ private_requirements_file }}"
  sudo_user: "{{ bharatmoocapp_user }}"
  environment:
    GIT_SSH: "{{ bharatmoocapp_git_ssh }}"
  when: BHARATMOOCAPP_INSTALL_PRIVATE_REQUIREMENTS
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"

# If using CAS and you have a function for mapping attributes, install
# the module here.  The next few tasks set up the python code sandbox
- name: install CAS attribute module
  pip: >
    name="{{ BHARATMOOCAPP_CAS_ATTRIBUTE_PACKAGE }}"
    virtualenv="{{bharatmoocapp_venv_dir}}"
    state=present
    extra_args="-i {{ COMMON_PYPI_MIRROR_URL }} --exists-action w --use-mirrors"
  sudo_user: "{{ bharatmoocapp_user }}"
  when: BHARATMOOCAPP_CAS_ATTRIBUTE_PACKAGE|length > 0
  notify: "restart bharatmoocapp"

# Install the sandbox python modules into {{ bharatmoocapp_venv_dir }}
- name : install sandbox requirements into regular venv
  # Need to use shell rather than pip so that we can maintain the context of our current working directory; some
  # requirements are pathed relative to the bharatmooc-platform repo. Using the pip from inside the virtual environment implicitly
  # installs everything into that virtual environment.
  shell: >
    {{ bharatmoocapp_venv_dir }}/bin/pip install -i {{ COMMON_PYPI_MIRROR_URL }} --exists-action w --use-mirrors -r {{ item }}
    chdir={{ bharatmoocapp_code_dir }}
  with_items:
  - "{{ sandbox_base_requirements }}"
  - "{{ sandbox_local_requirements }}"
  - "{{ sandbox_post_requirements }}"
  sudo_user: "{{ bharatmoocapp_user }}"
  when: "not BHARATMOOCAPP_PYTHON_SANDBOX and (not inst.stat.exists or new.stat.md5 != inst.stat.md5)"
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"

# The next few tasks install xml courses.

# Install the xml courses from an s3 bucket
- name: get s3 one time url
  s3: >
    bucket="{{ BHARATMOOCAPP_XML_S3_BUCKET }}"
    object="{{ BHARATMOOCAPP_XML_S3_KEY }}"
    mode="geturl"
    expiration=300
  when: not BHARATMOOCAPP_XML_FROM_GIT and BHARATMOOCAPP_XML_S3_BUCKET and BHARATMOOCAPP_XML_S3_KEY
  register: s3_one_time_url

- name: download from one time url
  get_url: url="{{ s3_one_time_url.url }}" dest="/tmp/{{ BHARATMOOCAPP_XML_S3_KEY|basename }}"
  when: not BHARATMOOCAPP_XML_FROM_GIT and BHARATMOOCAPP_XML_S3_BUCKET and BHARATMOOCAPP_XML_S3_KEY
  register: download_xml_s3

- name: unzip the data to the data dir
  shell: >
    tar xzf /tmp/{{ BHARATMOOCAPP_XML_S3_KEY|basename }}
    chdir="{{ bharatmoocapp_data_dir }}"
  when: download_xml_s3.changed

- include: xml.yml
  tags: deploy
  when: BHARATMOOCAPP_XML_FROM_GIT

# The next few tasks set up the python code sandbox

# need to disable this profile, otherwise the pip inside the sandbox venv has no permissions
# to install anything
- name: code sandbox | put sandbox apparmor profile in complain mode
  command: /usr/sbin/aa-complain /etc/apparmor.d/code.sandbox
  when: BHARATMOOCAPP_PYTHON_SANDBOX
  tags:
  - bharatmoocapp-sandbox

- name: code sandbox | Install base sandbox requirements and create sandbox virtualenv
  pip: >
    requirements="{{sandbox_base_requirements}}"
    virtualenv="{{bharatmoocapp_sandbox_venv_dir}}"
    state=present
    extra_args="-i {{ COMMON_PYPI_MIRROR_URL }} --exists-action w --use-mirrors"
  sudo_user: "{{ bharatmoocapp_sandbox_user }}"
  when: BHARATMOOCAPP_PYTHON_SANDBOX
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"
  tags:
  - bharatmoocapp-sandbox

- name: code sandbox | Install sandbox requirements into sandbox venv
  shell: >
    {{ bharatmoocapp_sandbox_venv_dir }}/bin/pip install -i {{ COMMON_PYPI_MIRROR_URL }} --exists-action w --use-mirrors -r {{ item }}
    chdir={{ bharatmoocapp_code_dir }}
  with_items:
  - "{{ sandbox_local_requirements }}"
  - "{{ sandbox_post_requirements }}"
  sudo_user: "{{ bharatmoocapp_sandbox_user }}"
  when: BHARATMOOCAPP_PYTHON_SANDBOX
  register: sandbox_install_output
  changed_when: sandbox_install_output.stdout is defined and 'installed' in sandbox_install_output.stdout
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"
  tags:
  - bharatmoocapp-sandbox

- name: code sandbox | put code sandbox into aa-enforce or aa-complain mode, depending on BHARATMOOCAPP_SANDBOX_ENFORCE
  command: /usr/sbin/{{ bharatmoocapp_aa_command }} /etc/apparmor.d/code.sandbox
  when: BHARATMOOCAPP_PYTHON_SANDBOX
  tags:
  - bharatmoocapp-sandbox

- name: compiling all py files in the bharatmooc-platform repo
  shell: "{{ bharatmoocapp_venv_bin }}/python -m compileall -x .git/.* {{ bharatmoocapp_code_dir }}"
  sudo_user: "{{ bharatmoocapp_user }}"
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"

  # alternative would be to give {{ common_web_user }} read access
  # to the virtualenv but that permission change will require
  # root access.
- name: give other read permissions to the virtualenv
  command: chmod -R o+r "{{ bharatmoocapp_venv_dir }}"
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"

- name: create checksum for installed requirements
  shell: cp /var/tmp/bharatmoocapp.req.new /var/tmp/bharatmoocapp.req.installed
  sudo_user: "{{ bharatmoocapp_user }}"
  notify: "restart bharatmoocapp"


# https://code.launchpad.net/~wligtenberg/django-openid-auth/mysql_fix/+merge/22726
# This is necessary for when syncdb is run and the django_openid_auth module is installed,
# not sure if this fix will ever get merged
- name: openid workaround
  shell: sed -i -e 's/claimed_id = models.TextField(max_length=2047, unique=True/claimed_id = models.TextField(max_length=2047/' {{ bharatmoocapp_venv_dir }}/lib/python2.7/site-packages/django_openid_auth/models.py
  when: openid_workaround is defined
  sudo_user: "{{ bharatmoocapp_user }}"
  notify:
  - "restart bharatmoocapp"
  - "restart bharatmoocapp_workers"

# creates the supervisor jobs for the
# service variants configured, runs
# gather_assets and db migrations
- include: service_variant_config.yml
  tags:
    - service_variant_config
    - deploy

  # call supervisorctl update. this reloads
  # the supervisorctl config and restarts
  # the services if any of the configurations
  # have changed.

- name: update supervisor configuration
  shell:  "{{ supervisor_ctl }} -c {{ supervisor_cfg }} update"
  register: supervisor_update
  sudo_user: "{{ supervisor_service_user }}"
  changed_when: supervisor_update.stdout is defined and supervisor_update.stdout != ""
  when: not disable_bharatmooc_services

- name: ensure bharatmoocapp has started
  supervisorctl_local: >
    state=started
    supervisorctl_path={{ supervisor_ctl }}
    config={{ supervisor_cfg }}
    name="bharatmoocapp:{{ item }}"
  sudo_user: "{{ supervisor_service_user }}"
  when: celery_worker is not defined and not disable_bharatmooc_services
  with_items: service_variants_enabled

- name: ensure bharatmoocapp_workers has started
  supervisorctl_local: >
    name="bharatmoocapp_worker:{{ item.service_variant }}_{{ item.queue }}_{{ item.concurrency }}"
    supervisorctl_path={{ supervisor_ctl }}
    config={{ supervisor_cfg }}
    state=started
  when: celery_worker is defined and not disable_bharatmooc_services
  with_items: bharatmoocapp_workers
  sudo_user: "{{ supervisor_service_user }}"

- name: create symlinks from the venv bin dir
  file: >
    src="{{ bharatmoocapp_venv_bin }}/{{ item }}"
    dest={{ COMMON_BIN_DIR }}/{{ item.split('.')[0] }}.bharatmoocapp
    state=link
  with_items:
  - python
  - pip
  - django-admin.py

- name: create symlinks from the repo dir
  file: >
    src="{{ bharatmoocapp_code_dir }}/{{ item }}"
    dest={{ COMMON_BIN_DIR }}/{{ item.split('.')[0] }}.bharatmoocapp
    state=link
  with_items:
  - manage.py

- name: remove read-only ssh key
  file: path={{ bharatmoocapp_git_identity }} state=absent
  when: BHARATMOOCAPP_USE_GIT_IDENTITY

- set_fact: bharatmoocapp_installed=true
