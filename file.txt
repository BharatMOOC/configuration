./cloudformation_templates/edx-server-single-instance.json
./cloudformation_templates/edx-server-multi-instance.json
./cloudformation_templates/edx-reference-architecture.json
./cloudformation_templates/edx-admin-reference-architecture.json
./playbooks/bharatmooc-east/edx_vpc.yml
./playbooks/bharatmooc-east/edx_provision.yml
./playbooks/bharatmooc-east/secure_example/vars/edxapp_ref_custom_vars.yml
./playbooks/bharatmooc-east/secure_example/vars/edxapp_ref_users.yml
./playbooks/bharatmooc-east/secure_example/vars/edxapp_ref_vars.yml
./playbooks/bharatmooc-east/secure_example/vars/edx_jenkins_tests.yml
./playbooks/bharatmooc-east/stop_all_edx_services.yml
./playbooks/bharatmooc-east/edx_mirror.yml
./playbooks/bharatmooc-east/edxapp.yml
./playbooks/bharatmooc-east/update_edxapp_db_users.yml
./playbooks/bharatmooc-east/edx_continuous_integration.yml
./playbooks/bharatmooc-east/edx_jenkins_tests.yml
./playbooks/bharatmooc-east/edxapp_migrate.yml
./playbooks/bharatmooc-east/files/edx-server-ubuntu-configuration.json
./playbooks/bharatmooc-east/edx_ansible.yml
./playbooks/bharatmooc-east/edx_notifier.yml
./playbooks/bharatmooc-east/roles/edxlocal/meta/main.yml
./playbooks/bharatmooc-east/roles/edxlocal/defaults/main.yml
./playbooks/bharatmooc-east/roles/edxlocal/tasks/main.yml
./playbooks/bharatmooc-east/roles/jenkins_admin/templates/edx/var/jenkins/jobs/backup-jenkins/config.xml.j2
./playbooks/bharatmooc-east/roles/jenkins_admin/templates/edx/var/jenkins/boto.j2
./playbooks/bharatmooc-east/roles/jenkins_admin/templates/edx/var/jenkins/hudson.plugins.s3.S3BucketPublisher.xml.j2
./playbooks/bharatmooc-east/roles/edxapp/templates/edxapp.conf.j2
./playbooks/bharatmooc-east/roles/edxapp/templates/cms.conf.j2
./playbooks/bharatmooc-east/roles/edxapp/templates/workers.conf.j2
./playbooks/bharatmooc-east/roles/edxapp/templates/code.sandbox.j2
./playbooks/bharatmooc-east/roles/edxapp/templates/course.xml.j2
./playbooks/bharatmooc-east/roles/edxapp/templates/95-sandbox-sudoer.j2
./playbooks/bharatmooc-east/roles/edxapp/templates/lms.auth.json.j2
./playbooks/bharatmooc-east/roles/edxapp/templates/cms.auth.json.j2
./playbooks/bharatmooc-east/roles/edxapp/templates/cms.env.json.j2
./playbooks/bharatmooc-east/roles/edxapp/templates/lms.conf.j2
./playbooks/bharatmooc-east/roles/edxapp/templates/git_ssh_auth.sh.j2
./playbooks/bharatmooc-east/roles/edxapp/templates/git_ssh_noauth.sh.j2
./playbooks/bharatmooc-east/roles/edxapp/templates/edxapp_env.j2
./playbooks/bharatmooc-east/roles/edxapp/templates/lms.env.json.j2
./playbooks/bharatmooc-east/roles/edxapp/meta/main.yml
./playbooks/bharatmooc-east/roles/edxapp/defaults/main.yml
./playbooks/bharatmooc-east/roles/edxapp/tasks/service_variant_config.yml
./playbooks/bharatmooc-east/roles/edxapp/tasks/deploy.yml
./playbooks/bharatmooc-east/roles/edxapp/tasks/main.yml
./playbooks/bharatmooc-east/roles/edxapp/tasks/xml.yml
./playbooks/bharatmooc-east/roles/edxapp/tasks/python_sandbox_env.yml
./playbooks/bharatmooc-east/roles/edxapp/handlers/main.yml
./playbooks/bharatmooc-east/roles/common/templates/etc/logrotate.d/hourly/edx_logrotate_tracking_log.j2
./playbooks/bharatmooc-east/roles/common/templates/etc/logrotate.d/hourly/edx_logrotate.j2
./playbooks/bharatmooc-east/roles/common/templates/edx_rsyslog.j2
./playbooks/bharatmooc-east/roles/stop_all_edx_services/defaults/main.yml
./playbooks/bharatmooc-east/roles/stop_all_edx_services/tasks/main.yml
./playbooks/bharatmooc-east/roles/stop_all_edx_services/handlers/main.yml
./playbooks/bharatmooc-east/roles/supervisor/templates/edx/app/supervisor/supervisord.conf.j2
./playbooks/bharatmooc-east/roles/edx_ansible/templates/update.j2
./playbooks/bharatmooc-east/roles/edx_ansible/templates/dumpall.yml.j2
./playbooks/bharatmooc-east/roles/edx_ansible/meta/main.yml
./playbooks/bharatmooc-east/roles/edx_ansible/defaults/main.yml
./playbooks/bharatmooc-east/roles/edx_ansible/tasks/deploy.yml
./playbooks/bharatmooc-east/roles/edx_ansible/tasks/main.yml
./playbooks/bharatmooc-east/roles/edx_service/meta/main.yml
./playbooks/bharatmooc-east/roles/edx_service/defaults/main.yml
./playbooks/bharatmooc-east/roles/edx_service/tasks/main.yml
./playbooks/bharatmooc-east/roles/edx_service/handlers/main.yml
./playbooks/bharatmooc-east/roles/elasticsearch/templates/edx/etc/elasticsearch/logging.yml.j2
./playbooks/bharatmooc-east/roles/elasticsearch/templates/edx/etc/elasticsearch/elasticsearch.yml.j2
./playbooks/bharatmooc-east/roles/xqwatcher/templates/edx/app/supervisor/conf.d/xqwatcher.conf.j2
./playbooks/bharatmooc-east/roles/xqwatcher/templates/edx/app/xqwatcher/conf.d/course.json.j2
./playbooks/bharatmooc-east/roles/xqwatcher/templates/edx/app/xqwatcher/data/requirements.txt.j2
./playbooks/bharatmooc-east/roles/xqwatcher/templates/edx/app/xqwatcher/xqwatcher.json.j2
./playbooks/bharatmooc-east/roles/analytics-api/templates/edx/app/supervisor/conf.d.available/analytics-api.conf.j2
./playbooks/bharatmooc-east/roles/analytics-api/templates/edx/app/analytics-api/analytics_api_env.j2
./playbooks/bharatmooc-east/roles/analytics-api/templates/edx/app/analytics-api/analytics-api.sh.j2
./playbooks/bharatmooc-east/roles/analytics-api/templates/edx/app/analytics-api/analytics-api.yaml.j2
./playbooks/bharatmooc-east/roles/nginx/templates/etc/logrotate.d/edx_logrotate_nginx_error.j2
./playbooks/bharatmooc-east/roles/nginx/templates/etc/logrotate.d/edx_logrotate_nginx_access.j2
./playbooks/bharatmooc-east/roles/nginx/templates/edx/app/nginx/robots.txt.j2
./playbooks/bharatmooc-east/roles/nginx/templates/edx/app/nginx/sites-available/basic-auth.j2
./playbooks/bharatmooc-east/roles/nginx/templates/edx/app/nginx/sites-available/nginx_redirect.j2
./playbooks/bharatmooc-east/roles/nginx/templates/edx/app/nginx/sites-available/cms.j2
./playbooks/bharatmooc-east/roles/nginx/templates/edx/app/nginx/sites-available/xserver.j2
./playbooks/bharatmooc-east/roles/nginx/templates/edx/app/nginx/sites-available/discern.j2
./playbooks/bharatmooc-east/roles/nginx/templates/edx/app/nginx/sites-available/lms.j2
./playbooks/bharatmooc-east/roles/nginx/templates/edx/app/nginx/sites-available/ora.j2
./playbooks/bharatmooc-east/roles/nginx/templates/edx/app/nginx/sites-available/robots.j2
./playbooks/bharatmooc-east/roles/nginx/templates/edx/app/nginx/sites-available/xqueue.j2
./playbooks/bharatmooc-east/roles/nginx/templates/edx/app/nginx/sites-available/devpi.j2
./playbooks/bharatmooc-east/roles/nginx/templates/edx/app/nginx/sites-available/gh_mirror.j2
./playbooks/bharatmooc-east/roles/nginx/templates/edx/app/nginx/sites-available/lms-preview.j2
./playbooks/bharatmooc-east/roles/nginx/templates/edx/app/nginx/sites-available/certs.j2
./playbooks/bharatmooc-east/roles/nginx/templates/edx/app/nginx/sites-available/forum.j2
./playbooks/bharatmooc-east/roles/nginx/templates/edx/app/nginx/sites-available/analytics-api.j2
./playbooks/bharatmooc-east/roles/nginx/templates/edx/app/nginx/sites-available/edx-release.j2
./playbooks/bharatmooc-east/roles/notifier/templates/edx/app/supervisor/conf.d/notifier-celery-workers.conf.j2
./playbooks/bharatmooc-east/roles/notifier/templates/edx/app/supervisor/conf.d/notifier-scheduler.conf.j2
./playbooks/secure_example/vars/edxapp_ref_custom_vars.yml
./playbooks/secure_example/vars/edxapp_ref_users.yml
./playbooks/secure_example/vars/edxapp_ref_vars.yml
./playbooks/secure_example/vars/edx_jenkins_tests.yml
./playbooks/bharatmooc-west/edxapp_rolling_example.yml
./playbooks/bharatmooc-west/secure_example/vars/edxapp_ref_custom_vars.yml
./playbooks/bharatmooc-west/secure_example/vars/edxapp_ref_users.yml
./playbooks/bharatmooc-west/secure_example/vars/edxapp_ref_vars.yml
./playbooks/bharatmooc-west/secure_example/vars/edx_jenkins_tests.yml
./playbooks/bharatmooc-west/files/edx-server-ubuntu-configuration.json
./playbooks/bharatmooc-west/roles/edxlocal/meta/main.yml
./playbooks/bharatmooc-west/roles/edxlocal/defaults/main.yml
./playbooks/bharatmooc-west/roles/edxlocal/tasks/main.yml
./playbooks/bharatmooc-west/roles/jenkins_admin/templates/edx/var/jenkins/jobs/backup-jenkins/config.xml.j2
./playbooks/bharatmooc-west/roles/jenkins_admin/templates/edx/var/jenkins/boto.j2
./playbooks/bharatmooc-west/roles/jenkins_admin/templates/edx/var/jenkins/hudson.plugins.s3.S3BucketPublisher.xml.j2
./playbooks/bharatmooc-west/roles/edxapp/templates/edxapp.conf.j2
./playbooks/bharatmooc-west/roles/edxapp/templates/cms.conf.j2
./playbooks/bharatmooc-west/roles/edxapp/templates/workers.conf.j2
./playbooks/bharatmooc-west/roles/edxapp/templates/code.sandbox.j2
./playbooks/bharatmooc-west/roles/edxapp/templates/course.xml.j2
./playbooks/bharatmooc-west/roles/edxapp/templates/95-sandbox-sudoer.j2
./playbooks/bharatmooc-west/roles/edxapp/templates/lms.auth.json.j2
./playbooks/bharatmooc-west/roles/edxapp/templates/cms.auth.json.j2
./playbooks/bharatmooc-west/roles/edxapp/templates/cms.env.json.j2
./playbooks/bharatmooc-west/roles/edxapp/templates/lms.conf.j2
./playbooks/bharatmooc-west/roles/edxapp/templates/git_ssh_auth.sh.j2
./playbooks/bharatmooc-west/roles/edxapp/templates/git_ssh_noauth.sh.j2
./playbooks/bharatmooc-west/roles/edxapp/templates/edxapp_env.j2
./playbooks/bharatmooc-west/roles/edxapp/templates/lms.env.json.j2
./playbooks/bharatmooc-west/roles/edxapp/meta/main.yml
./playbooks/bharatmooc-west/roles/edxapp/defaults/main.yml
./playbooks/bharatmooc-west/roles/edxapp/tasks/service_variant_config.yml
./playbooks/bharatmooc-west/roles/edxapp/tasks/deploy.yml
./playbooks/bharatmooc-west/roles/edxapp/tasks/main.yml
./playbooks/bharatmooc-west/roles/edxapp/tasks/xml.yml
./playbooks/bharatmooc-west/roles/edxapp/tasks/python_sandbox_env.yml
./playbooks/bharatmooc-west/roles/edxapp/handlers/main.yml
./playbooks/bharatmooc-west/roles/common/templates/etc/logrotate.d/hourly/edx_logrotate_tracking_log.j2
./playbooks/bharatmooc-west/roles/common/templates/etc/logrotate.d/hourly/edx_logrotate.j2
./playbooks/bharatmooc-west/roles/common/templates/edx_rsyslog.j2
./playbooks/bharatmooc-west/roles/supervisor/templates/edx/app/supervisor/supervisord.conf.j2
./playbooks/bharatmooc-west/roles/elasticsearch/templates/edx/etc/elasticsearch/logging.yml.j2
./playbooks/bharatmooc-west/roles/elasticsearch/templates/edx/etc/elasticsearch/elasticsearch.yml.j2
./playbooks/bharatmooc-west/roles/xqwatcher/templates/edx/app/supervisor/conf.d/xqwatcher.conf.j2
./playbooks/bharatmooc-west/roles/xqwatcher/templates/edx/app/xqwatcher/conf.d/course.json.j2
./playbooks/bharatmooc-west/roles/xqwatcher/templates/edx/app/xqwatcher/data/requirements.txt.j2
./playbooks/bharatmooc-west/roles/xqwatcher/templates/edx/app/xqwatcher/xqwatcher.json.j2
./playbooks/bharatmooc-west/roles/nginx/templates/etc/logrotate.d/edx_logrotate_nginx_error.j2
./playbooks/bharatmooc-west/roles/nginx/templates/etc/logrotate.d/edx_logrotate_nginx_access.j2
./playbooks/bharatmooc-west/roles/nginx/templates/bharatmooc/app/nginx/sites-available/edx-release.j2
./playbooks/edx_sandbox.yml
./playbooks/files/edx-server-ubuntu-configuration.json
./playbooks/roles/bharatmoocapp/templates/edxapp.conf.j2
./playbooks/roles/bharatmoocapp/templates/edxapp_env.j2
./playbooks/roles/edxlocal/meta/main.yml
./playbooks/roles/edxlocal/defaults/main.yml
./playbooks/roles/edxlocal/tasks/main.yml
./playbooks/roles/jenkins_admin/templates/edx/var/jenkins/jobs/backup-jenkins/config.xml.j2
./playbooks/roles/jenkins_admin/templates/edx/var/jenkins/boto.j2
./playbooks/roles/jenkins_admin/templates/edx/var/jenkins/hudson.plugins.s3.S3BucketPublisher.xml.j2
./playbooks/roles/common/templates/etc/logrotate.d/hourly/edx_logrotate_tracking_log.j2
./playbooks/roles/common/templates/etc/logrotate.d/hourly/edx_logrotate.j2
./playbooks/roles/common/templates/edx_rsyslog.j2
./playbooks/roles/supervisor/templates/edx/app/supervisor/supervisord.conf.j2
./playbooks/roles/elasticsearch/templates/edx/etc/elasticsearch/logging.yml.j2
./playbooks/roles/elasticsearch/templates/edx/etc/elasticsearch/elasticsearch.yml.j2
./playbooks/roles/xqwatcher/templates/edx/app/supervisor/conf.d/xqwatcher.conf.j2
./playbooks/roles/xqwatcher/templates/edx/app/xqwatcher/conf.d/course.json.j2
./playbooks/roles/xqwatcher/templates/edx/app/xqwatcher/data/requirements.txt.j2
./playbooks/roles/xqwatcher/templates/edx/app/xqwatcher/xqwatcher.json.j2
./playbooks/roles/analytics-api/templates/edx/app/supervisor/conf.d.available/analytics-api.conf.j2
./playbooks/roles/analytics-api/templates/edx/app/analytics-api/analytics_api_env.j2
./playbooks/roles/analytics-api/templates/edx/app/analytics-api/analytics-api.sh.j2
./playbooks/roles/analytics-api/templates/edx/app/analytics-api/analytics-api.yaml.j2
./playbooks/roles/nginx/templates/etc/logrotate.d/edx_logrotate_nginx_error.j2
./playbooks/roles/nginx/templates/etc/logrotate.d/edx_logrotate_nginx_access.j2
./playbooks/roles/nginx/templates/bharatmooc/app/nginx/sites-available/edx-release.j2
